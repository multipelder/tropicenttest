export const mutations = {
    SET_PROFILE: 'modules/auth/SET_PROFILE',
    ADD_PROFILE: 'modules/auth/ADD_PROFILE',
};

export const actions = {
    register: 'modules/auth/register',
    login: 'modules/auth/login',
};

export default {
    state: {
        profile: null,
        registeredProfiles: [
            { email: 'user1', password: '1111' },
        ],
    },

    mutations: {
        [mutations.SET_PROFILE]: (state, profile) => {
            state.profile = profile;
        },
        [mutations.ADD_PROFILE]: (state, profile) => {
            state.registeredProfiles.push(profile);
        }
    },
    
    actions: {
        [actions.register]({ state, commit }, payload) {
            let promise = new Promise((resolve, reject) => {

                let profile = state.registeredProfiles.filter((profile) => {
                    if (profile.email === payload.email) {
                        return profile;
                    }
                });
    
                if (profile.length > 0) {
                    let error = new Error('Пользователь уже существует');
                    reject(error);
                }
    
                profile = {
                    email: payload.email, 
                    password: payload.password, 
                };
                
                commit(mutations.ADD_PROFILE, profile);
                commit(mutations.SET_PROFILE, profile);
                
                resolve(profile);
            })
            return promise;
        },

        [actions.login]: ({ state, commit }, payload) => {

            let promise = new Promise((resolve, reject) => {
                const filteredProfiles = state.registeredProfiles.filter((profile) => {
                    if (profile.email === payload.email) {
                        return profile;
                    }
                });
                
                if (filteredProfiles.length == 0) {
                    let error = new Error('Пользователь не зарегистрирован');
                    reject(error);
                }

                const profile = filteredProfiles[0];
                
                const isCorrectPassword = profile.password === payload.password;
                if (!isCorrectPassword) {
                    let error = new Error('Неверный пароль');
                    reject(error);
                }

                commit(mutations.SET_PROFILE, profile);

                resolve(profile);
            })
            return promise;
        }
    },

    getters: {
        profile(state) {
            return state.profile;
        },
        registeredProfiles(state) {
            return state.registeredProfiles;
        }
    },
};
