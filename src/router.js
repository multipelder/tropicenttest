import Vue from 'vue';
import VueRouter from 'vue-router';

import Authorization from './components/Authorization.vue';
import Registration from './components/Registration.vue';

Vue.use(VueRouter);

const router = new VueRouter({
    routes: [
        {
            path: '/authorization',
            name: 'authorization',
            component: Authorization,
        },
        {
            path: '/registration',
            name: 'registration',
            component: Registration,
        },
        {
            path: '*',
            redirect: 'authorization',
        }
    ]
});

export default router;