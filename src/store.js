import Vue from 'vue';
import Vuex from 'vuex';

import authModule from './modules/auth.js';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {},
    mutations: {},
    actions: {},
    modules: [
        authModule,
    ]
});

export default store;
